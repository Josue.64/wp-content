<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Astra
 * @since 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header(); ?>

<?php if ( astra_page_layout() == 'left-sidebar' ) : ?>

	<?php get_sidebar(); ?>

<?php endif ?>

	<div id="primary" <?php astra_primary_class(); ?>>

		<?php astra_primary_content_top(); ?>

		<?php //astra_content_loop(); ?>

		<?php if(have_posts()) : 
			while (have_posts()) :
			 the_post(); ?>
				<h2 class="post-title titulo"><?php the_title() ?></h2>
				<div>    
				<?php the_content(); ?>

                <p class="info"><?php echo get_field('s1'); ?></p>
                <?php $foto = get_field('i1'); ?>
                <img class="imagen-proyecto" src="<?php echo $foto['url'] ?>" alt="<?php echo $foto['alt'] ?>">
				<p class="info"><?php echo get_field('d1'); ?></p>

				<p class="info2"><?php echo get_field('s2'); ?></p>
                <?php $foto = get_field('i2'); ?>
                <img class="imagen-proyecto2" src="<?php echo $foto['url'] ?>" alt="<?php echo $foto['alt'] ?>">
				<p class="info2"><?php echo get_field('d2'); ?></p>

				<p class="info3"><?php echo get_field('s3'); ?></p>
                <?php $foto = get_field('i3'); ?>
                <img class="imagen-proyecto3" src="<?php echo $foto['url'] ?>" alt="<?php echo $foto['alt'] ?>">
				<p class="info3"><?php echo get_field('d3'); ?></p>

				</div>
		<?php endwhile;
		endif; ?>

		<?php astra_primary_content_bottom(); ?>

	</div><!-- #primary -->

<?php if ( astra_page_layout() == 'right-sidebar' ) : ?>

	<?php get_sidebar(); ?>

<?php endif ?>

<?php get_footer(); ?>
